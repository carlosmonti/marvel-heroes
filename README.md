# Technical challenge - Marvel Heroes

## Frontend Challenge

Hi,
We’re excited that you decided to participate in this challenge.
The goal is to build a website that shows a list of Marvel superheroes with their respective detail pages. How you solve the design/UI is up to you.
Create an account at [https://developer.marvel.com/account](https://developer.marvel.com/account), then use the API to fetch a list of superheroes and display them. The app should allow client-side search by name, comics, stories, and series.
Each of the superheroes should have their own page where an image of the superhero, a list of comics, series, etc. is shown.

### What we will evaluate

- Clean, scalable and reusable code.
- End result from a user perspective.
- Responsive.

### Bonus Points (optional)

- In the main page you can modify the information of a superhero, delete them, and save
to favorites, which must be done using localStorage so it persists when the page is
reloaded.
- Publish the page (for example github pages or vercel app) and send the link so it can be
viewed by anyone.
- Use TypeScript.

We might ask you to give us a walkthrough of how you solved this challenge to get a better notion of how you think. If you need to use any external resource (framework, library, code snippet, etc.), please justify why they were used and chosen in the first place, and provide a link to the resource you used.

## Challenge output

[Preview](https://carlosmonti.gitlab.io/marvel-heroes/)

### Desktop / Tablet

![desktop-tablet](/uploads/99c7d9fd8dd4981e75af2b425cbb007b/desktop-tablet.gif)

### Mobile

![mobile](/uploads/7d19d2e0b29a4d949c0d5b9c01c7137e/mobile.gif)

### Framework, library, code snippet, etc. used

- [CRA5](https://github.com/facebook/create-react-app): Create React apps with no build configuration..
- [ESLint](https://github.com/eslint/eslint): a tool for identifying and reporting on patterns found in ECMAScript/JavaScript code.
- [Prettier](https://github.com/prettier/prettier): an opinionated code formatter.
- [Chakra-ui](https://github.com/chakra-ui/chakra-ui): set of accessible, reusable, and composable React components that make it super easy to create websites and apps.
- [crypto-js/md5](https://github.com/brix/crypto-js): JavaScript library of crypto standards.
- [react-icons](https://github.com/react-icons/react-icons): popular icons in your React projects.
