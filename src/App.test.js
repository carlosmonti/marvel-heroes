import { render, screen } from '@testing-library/react';
import App from './App';

test('renders some elements', () => {
  render(<App />);
  const categoryElement = screen.getByText(/Characters/i);
  const searchElement = screen.getByText(/Search/i);
  expect(categoryElement).toBeInTheDocument();
  expect(searchElement).toBeInTheDocument();
});
