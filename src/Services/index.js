import MD5 from 'crypto-js/md5';

const BASE_URL = 'https://gateway.marvel.com:443';
const PUBLIC_KEY = process.env.REACT_APP_MARVEL_PUBLIC_KEY;
const PRIVATE_KEY = process.env.REACT_APP_MARVEL_PRIVATE_KEY;

const ts = new Date().getTime();
const hash = MD5(ts + PRIVATE_KEY + PUBLIC_KEY).toString();

const appendAuthentication = (url) => {
  url.searchParams.append('apikey', PUBLIC_KEY);
  url.searchParams.append('ts', ts);
  url.searchParams.append('hash', hash);

  return url;
};

export const getCharacters = (nameStartsWith, orderBy, limit, offset) => {
  console.info('getCharacters: ', { nameStartsWith, orderBy, limit, offset });
  const url = new URL('/v1/public/characters', BASE_URL);

  if(nameStartsWith) {
    url.searchParams.append('nameStartsWith', nameStartsWith);
  }
  if(orderBy) {
    url.searchParams.append('orderBy', orderBy);
  }
  if(limit) {
    url.searchParams.append('limit', limit);
  }
  if(offset) {
    url.searchParams.append('offset', offset);
  }

  return fetch(appendAuthentication(url))
    .then(response => response.json())
    .catch(err => console.log('Request Failed', err));
};

export const getCharacter = (id) => {
  console.info('getCharacter: ', { id });
  const url = new URL('/v1/public/characters/' + id, BASE_URL);

  return fetch(appendAuthentication(url))
    .then(response => response.json())
    .catch(err => console.log('Request Failed', err));
};

export const getComics = (titleStartsWith, orderBy, limit, offset) => {
  console.info('getComics: ', { titleStartsWith, orderBy, limit, offset });
  const url = new URL('/v1/public/comics', BASE_URL);
  if(titleStartsWith) {
    url.searchParams.append('titleStartsWith', titleStartsWith);
  }
  if(orderBy) {
    url.searchParams.append('orderBy', orderBy);
  }
  if(limit) {
    url.searchParams.append('limit', limit);
  }
  if(offset) {
    url.searchParams.append('offset', offset);
  }

  return fetch(appendAuthentication(url))
    .then(response => response.json())
    .catch(err => console.log('Request Failed', err));
};

export const getComic = (id) => {
  console.info('getComic: ', { id });
  const url = new URL('/v1/public/comics/' + id, BASE_URL);

  return fetch(appendAuthentication(url))
    .then(response => response.json())
    .catch(err => console.log('Request Failed', err));
};

export const getStories = (orderBy, limit, offset) => {
  console.info('getStories: ', { orderBy, limit, offset });
  const url = new URL('/v1/public/stories', BASE_URL);

  if(orderBy) {
    url.searchParams.append('orderBy', orderBy);
  }
  if(limit) {
    url.searchParams.append('limit', limit);
  }
  if(offset) {
    url.searchParams.append('offset', offset);
  }

  return fetch(appendAuthentication(url))
    .then(response => response.json())
    .catch(err => console.log('Request Failed', err));
};

export const getStory = (id) => {
  console.info('getStory: ', { id });
  const url = new URL('/v1/public/stories/' + id, BASE_URL);

  return fetch(appendAuthentication(url))
    .then(response => response.json())
    .catch(err => console.log('Request Failed', err));
};

export const getSeries = (titleStartsWith, orderBy, limit, offset) => {
  console.info('getSeries: ', { titleStartsWith, orderBy, limit, offset });
  const url = new URL('/v1/public/series', BASE_URL);

  if(titleStartsWith) {
    url.searchParams.append('titleStartsWith', titleStartsWith);
  }
  if(orderBy) {
    url.searchParams.append('orderBy', orderBy);
  }
  if(limit) {
    url.searchParams.append('limit', limit);
  }
  if(offset) {
    url.searchParams.append('offset', offset);
  }

  return fetch(appendAuthentication(url))
    .then(response => response.json())
    .catch(err => console.log('Request Failed', err));
};

export const getSerie = (id) => {
  console.info('getStory: ', { id });
  const url = new URL('/v1/public/series/' + id, BASE_URL);

  return fetch(appendAuthentication(url))
    .then(response => response.json())
    .catch(err => console.log('Request Failed', err));
};
