import { Flex } from '@chakra-ui/react';
import { Routes, Route, Navigate } from 'react-router-dom';
import './index.css';

import Header from './Components/Header';
import Home from './Page/Home';
import Detail from './Page/Detail';

export const CATEGORIES = [
  'characters',
  'comics',
  'stories',
  'series'
];
export const LIMIT_DEFAULT_VALUE = 10;
export const LIMIT = [LIMIT_DEFAULT_VALUE, 30, 50];
export const MAX_INPUT_WIDTH = 500;

const App = () => {
  return (
    <Flex direction="column" h="100%">
      <Header />
      <Routes>
        <Route path="/" element={<Navigate to="characters" replace />} />
        {CATEGORIES.map((category) => {
          return (
            <Route key={category} path={category}>
              <Route path=":id" element={<Detail category={category} replace />} />
              <Route index element={<Home category={category} />} />
            </Route>
          );
        })}
        <Route
          path="*"
          element={<Navigate to="/" replace />}
        />
      </Routes>
    </Flex>
  );
};

export default App;
