import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { Flex, Spinner } from '@chakra-ui/react';

import Category from '../../Components/Category';
import ItemList from '../../Components/ItemList';
import Limit from '../../Components/Limit';
import SearchBar from '../../Components/SearchBar';

import {
  CATEGORIES,
  LIMIT_DEFAULT_VALUE
} from '../../App';
import { getCharacters, getComics, getSeries, getStories } from '../../Services';
import { useNavigate } from 'react-router-dom';

const Home = ({ category }) => {
  const [categoryState, setCategoryState] = useState(category || CATEGORIES[0]);
  const [search, setSearch] = useState('');
  const [limit, setLimit] = useState(LIMIT_DEFAULT_VALUE);
  const [offset, setOffset] = useState(0);
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const isCategoryStories = category !== CATEGORIES[2];

  let navigate = useNavigate();

  const handleCategory = (value) => {
    setCategoryState(value);
    navigate(`/${value}`);
  };

  const handleSearchSubmit = () => {
    getData(categoryState, search, limit, offset);
  };

  const handleLimitChange = (newLimit) => {
    setLimit(newLimit);
    handleSearchSubmit();
  };

  const handleInputChange = (event) => {
    const { target: { value } } = event;
    setSearch(value);
  };

  // eslint-disable-next-line no-unused-vars
  const handleOffsetChange = (value) => setOffset(value);

  const getData = (value, search, limit, offset) => {
    setIsLoading(true);
    switch (value) {
    case CATEGORIES[0]:
      getCharacters(search, 'name', limit, offset)
        .then(response => {
          const { data: { results } } = response;
          setIsLoading(false);
          setData(results);
        })
        .catch(error => {
          setIsLoading(false);
          console.error({ error });
        });
      break;
    case CATEGORIES[1]:
      getComics(search, 'title', limit, offset)
        .then(response => {
          const { data: { results } } = response;
          setIsLoading(false);
          setData(results);
        })
        .catch(error => {
          setIsLoading(false);
          console.error({ error });
        });
      break;
    case CATEGORIES[2]:
      setSearch('');
      getStories('id', limit, offset)
        .then(response => {
          const { data: { results } } = response;
          setIsLoading(false);
          setData(results);
        })
        .catch(error => {
          setIsLoading(false);
          console.error({ error });
        });
      break;
    case CATEGORIES[3]:
      getSeries(search, 'title', limit, offset)
        .then(response => {
          const { data: { results } } = response;
          setIsLoading(false);
          setData(results);
        })
        .catch(error => {
          setIsLoading(false);
          console.error({ error });
        });
      break;
    }
  };

  useEffect(() => {
    getData(categoryState, search, limit, offset);
  }, [categoryState, limit, offset]);

  return (
    <Flex direction="column" h="100%">
      <Flex direction="column" justify="center" p="20px 20px">
        <Category value={categoryState} onChange={handleCategory} />
        {isCategoryStories && (
          <SearchBar
            category={categoryState}
            search={search}
            handleSubmit={handleSearchSubmit}
            onChange={handleInputChange}
            value={search}
          />
        )}
        <Limit value={limit} onChange={handleLimitChange} />
      </Flex>
      {isLoading && (
        <Flex w="100%" justify="center" align="center" h="100%">
          <Spinner color="brand.100" size='xl' thickness="4px" />
        </Flex>
      )}
      {!isLoading && <ItemList data={data} category={categoryState} />}
    </Flex>
  );
};

const { string } = PropTypes;

Home.propTypes = {
  category: string
};

export default Home;
