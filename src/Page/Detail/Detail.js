import PropTypes from 'prop-types';
import { TiArrowBack } from 'react-icons/ti';
import { Center, Flex, IconButton, Image, Spinner, useColorModeValue } from '@chakra-ui/react';
import { useNavigate, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { getCharacter, getComic, getSerie, getStory } from '../../Services';
import { CATEGORIES } from '../../App';
import { default as not_found } from './images/notfound.svg';
import DetailContent from './DetailContent';
import ErrorBoundary from '../../Components/ErrorBoundary';

const Detail = ({ category }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [details, setDetails] = useState(null);

  let { id } = useParams();
  let navigate = useNavigate();
  const iconColor = useColorModeValue('brand.100', 'white');


  const getData = async (cat, id) => {
    let data;

    switch (cat) {
    case CATEGORIES[0]: // character
      data = await getCharacter(id);
      break;

    case CATEGORIES[1]: // comic
      data = await getComic(id);
      break;

    case CATEGORIES[2]: // story
      data = await getStory(id);
      break;

    case CATEGORIES[3]: // serie
      data = await getSerie(id);
      break;

    default:
      data = await getCharacter(id);
      break;
    }

    if (data.code && data.code === 200) {
      const { data: { results } } = data;
      data = { ...results[0] };
      setDetails(data);
    } else {
      setDetails('error');
    }
    setIsLoading(false);
  };

  useEffect(() => {
    try {
      getData(category, id);
    } catch (error) {
      setIsLoading(false);
      setDetails('error');
      console.error({ error });
    }
  }, []);

  if (isLoading) {
    return (
      <Center h="100%">
        <Spinner color="brand.100" size='xl' thickness="4px" />
      </Center>
    );
  } else {
    if(details) {
      if (typeof details === 'object') {
        return (
          <>
            <Flex m="20px 0 20px 20px">
              <IconButton
                aria-label='Go back'
                color={iconColor}
                icon={<TiArrowBack />}
                onClick={() => navigate(-1)}
                size="lg"
                variant="ghost"
              />
            </Flex>
            <Center>
              <ErrorBoundary>
                <DetailContent data={details} />
              </ErrorBoundary>
            </Center>
          </>
        );
      }

      if (typeof details === 'string') {
        return (
          <Center h="100%">
            <Image src={not_found} w={['90%', '50%']} color="brand.100" />
          </Center>
        );
      }
    }
  }
};


const { string } = PropTypes;

Detail.propTypes = {
  category: string
};


export default Detail;
