/* eslint-disable no-unused-vars */
import { Accordion, AccordionButton, AccordionIcon, AccordionItem, AccordionPanel, AspectRatio, Box, Flex, Heading, Image, Text, useColorModeValue } from '@chakra-ui/react';
import PropTypes from 'prop-types';
import CategoryInfo from '../../Components/CategoryInfo';

const DetailContent = ({ data }) => {
  console.log('DetailContent: ', { data });
  const { name, title, description, thumbnail, comics, series, stories, characters } = data;
  const borderColor = useColorModeValue('black', '#3f444e');
  const headingColor = useColorModeValue('brand.100', 'white');
  const imageSrc = thumbnail
    ? `${thumbnail.path}/standard_fantastic.${thumbnail.extension}`
    : null;
  const paddingConfig = [5, '20px'];

  return (
    <Flex direction="column" p={paddingConfig} flex={1} maxW="840px">
      <Flex borderColor={borderColor} borderWidth={['10px', '6px']} maxW={[null, 400]} w="100%" alignSelf="center">
        <AspectRatio ratio={[5/4]} flex={1}>
          <Image
            src={imageSrc}
            fallbackSrc='http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg'
          />
        </AspectRatio>
      </Flex>
      <Flex direction="column" mt={paddingConfig} mb={paddingConfig}>
        <Heading fontSize='2xl' mb={[paddingConfig]}>
          {name || title}
        </Heading>
        {description && (<Text>{description}</Text>)}
      </Flex>
      <Accordion allowMultiple>
        {!!(characters && characters.available > 0) && (
          <AccordionItem>
            <h2>
              <AccordionButton>
                <Box flex='1' textAlign='justify'>
                  <Heading fontSize='2xl' color={headingColor}>
                    Characters
                  </Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
            </h2>
            <AccordionPanel pb={4}>
              <CategoryInfo data={characters} />
            </AccordionPanel>
          </AccordionItem>
        )}
        {!!(comics && comics.available > 0) && (
          <AccordionItem>
            <h2>
              <AccordionButton>
                <Box flex='1' textAlign='left'>
                  <Heading fontSize='2xl' color={headingColor}>
                    Comics
                  </Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
            </h2>
            <AccordionPanel pb={4}>
              <CategoryInfo data={comics} />
            </AccordionPanel>
          </AccordionItem>
        )}
        {!!(series && series.available > 0) && (
          <AccordionItem>
            <h2>
              <AccordionButton>
                <Box flex='1' textAlign='left'>
                  <Heading fontSize='2xl' color={headingColor}>
                    Series
                  </Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
            </h2>
            <AccordionPanel pb={4}>
              <CategoryInfo data={series} />
            </AccordionPanel>
          </AccordionItem>
        )}
        {!!(stories && stories.available > 0) && (
          <AccordionItem>
            <h2>
              <AccordionButton>
                <Box flex='1' textAlign='left'>
                  <Heading fontSize='2xl' color={headingColor}>
                    Stories
                  </Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
            </h2>
            <AccordionPanel pb={4}>
              <CategoryInfo data={stories} />
            </AccordionPanel>
          </AccordionItem>
        )}
      </Accordion>
    </Flex>
  );
};

const { shape } = PropTypes;

DetailContent.propTypes = {
  data: shape({})
};

export default DetailContent;
