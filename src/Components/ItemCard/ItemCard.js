/* eslint-disable no-unused-vars */
import PropTypes from 'prop-types';
import {
  AspectRatio, Flex, Image, LinkBox, Text, LinkOverlay, useColorModeValue
} from '@chakra-ui/react';
import { Link } from 'react-router-dom';

const ItemCard = ({ data, category }) => {
  const {
    id, name, title, thumbnail
  } = data;

  const imageSrc = thumbnail
    ? `${thumbnail.path}/standard_fantastic.${thumbnail.extension}`
    : null;

  const borderColor = useColorModeValue('black', '#3f444e');

  return (
    <LinkBox
      as="article"
      borderColor={borderColor}
      borderWidth={['10px', '6px']}
      flex={1}
      maxW='md'
      overflow="hidden"
      position="relative"
      style={{ display: 'block' }}
      w="100%"
    >
      <LinkOverlay as={Link} to={`/${category}/${id}`}>
        <AspectRatio ratio={5/4} flex={1}>
          <Image
            fallbackSrc='http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg'
            src={imageSrc}
          />
        </AspectRatio>
        <Text
          bg="white"
          border="2px solid #000"
          bottom="-2px"
          color="black"
          fontWeight={700}
          margin={0}
          padding="0 12px"
          position="absolute"
          right="-10px"
          size='lg'
          textAlign="right"
          transform="skew(-15deg)"
        >
          {name || title}
        </Text>
      </LinkOverlay>
    </LinkBox>
  );
};

const { arrayOf, number, shape, string } = PropTypes;

ItemCard.propTypes = {
  data: shape({
    comics: shape({}),
    description: string,
    events: shape({}),
    id: number,
    name: string,
    series: shape({}),
    stories: shape({}),
    thumbnail: shape({}),
    title: string,
    urls: arrayOf(shape({}))
  }),
  category: string
};

export default ItemCard;
