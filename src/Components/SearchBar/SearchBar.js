import PropTypes from 'prop-types';
import { Button, Flex, Input, InputGroup, InputRightAddon } from '@chakra-ui/react';
import { MAX_INPUT_WIDTH } from '../../App';

const SearchBar = ({
  category,
  handleSubmit,
  onChange,
  value
}) => {
  return (
    <Flex
      align="center"
      justify="center"
      direction="column"
      w="100%"
      alignSelf="center"
      marginBottom="10px"
    >
      <InputGroup size='lg' w={['100%', MAX_INPUT_WIDTH]}>
        <Input
          onChange={onChange}
          onKeyPress={e=> {
            if (e.key === 'Enter') {
              handleSubmit();
            }
          }}
          placeholder={`search for ${category}`}
          value={value}
        />
        <InputRightAddon>
          <Button variant="ghost" onClick={handleSubmit}>
            Search
          </Button>
        </InputRightAddon>
      </InputGroup>
    </Flex>
  );
};

const { string, func } = PropTypes;

SearchBar.propTypes = {
  category: string,
  handleSubmit: func,
  onChange: func,
  value: string
};

export default SearchBar;
