import PropTypes from 'prop-types';
import { Box, useColorModeValue, useRadio } from '@chakra-ui/react';

const CategoryRadioButton = ({ children, ...rest }) => {
  const { getInputProps, getCheckboxProps } = useRadio(rest);

  const input = getInputProps();
  const checkbox = getCheckboxProps();

  const bg = useColorModeValue('#b80000', '#3f444e');

  return (
    <Box as='label'>
      <input {...input} />
      <Box
        {...checkbox}
        borderRadius='md'
        borderWidth='1px'
        boxShadow='md'
        cursor='pointer'
        style={{ textTransform: 'capitalize', userSelect: 'none' }}
        px={3}
        py={3}
        _checked={{
          bg,
          color: 'white',
          fontWeight: '600'
        }}
        _focus={{
          boxShadow: 'outline',
        }}
      >
        {children}
      </Box>
    </Box>
  );
};

const { oneOfType, node } = PropTypes;

CategoryRadioButton.propTypes = {
  children: oneOfType([
    node.isRequired
  ]).isRequired
};

export default CategoryRadioButton;
