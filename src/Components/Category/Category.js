import PropTypes from 'prop-types';
import { Flex, HStack, useRadioGroup } from '@chakra-ui/react';

import CategoryRadioButton from './CategoryRadioButton';

import { CATEGORIES } from '../../App';

const Category = ({ onChange, value }) => {
  const { getRootProps, getRadioProps, setValue } = useRadioGroup({
    name: 'category',
    defaultValue: value || CATEGORIES[0],
  });

  const group = getRootProps();
  const handleCategoryChange = (event) => {
    const { value: category } = event.target;
    setValue(category);
    onChange(category);
  };

  return (
    <Flex align="center" justify="center" w="100%" marginBottom="20px">
      <HStack {...group}>
        {CATEGORIES.map((item) => {
          const radio = getRadioProps({ value: item });
          return (
            <CategoryRadioButton
              key={item}
              value={item}
              {...radio}
              onChange={handleCategoryChange}
            >
              {item}
            </CategoryRadioButton>
          );
        })}
      </HStack>
    </Flex>
  );
};

const { func, string } = PropTypes;

Category.propTypes = {
  onChange: func,
  value: string
};

export default Category;
