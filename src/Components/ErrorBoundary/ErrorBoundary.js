import React from 'react';
import PropTypes from 'prop-types';
import { Center, Text } from '@chakra-ui/react';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    console.error('Error Boundary: ', { error, errorInfo });
  }

  render() {
    const { children } = this.props;
    const { hasError } = this.state;

    if (hasError) {
      // You can render any custom fallback UI
      return (
        <Center>
          <Text color="brand.100" fontSize="2xl">Something went wrong.</Text>
        </Center>
      );
    }

    return children;
  }
}

const { oneOfType, node } = PropTypes;

ErrorBoundary.propTypes = {
  children: oneOfType([
    node.isRequired
  ]).isRequired
};

export default ErrorBoundary;
