import { useRef, useState, useEffect } from 'react';
import {
  Box,
  chakra,
  Flex,
  HStack,
  IconButton,
  Image,
  useColorMode,
  useColorModeValue
} from '@chakra-ui/react';
import { useViewportScroll } from 'framer-motion';
import { FaMoon, FaSun} from 'react-icons/fa';

import { default as logo_white } from './images/marvel_logo_white.svg';
import { default as logo_red } from './images/marvel_logo_red.svg';
import { Link } from 'react-router-dom';

const Header = () => {
  const { toggleColorMode: toggleMode } = useColorMode();
  const text = useColorModeValue('dark', 'light');
  const SwitchIcon = useColorModeValue(FaMoon, FaSun);

  const bg = useColorModeValue('#b80000', '#000');
  const border_top = useColorModeValue('brand.200', '#222');
  const logo = useColorModeValue(logo_white, logo_red);
  const ref = useRef();
  const [y, setY] = useState(0);
  const { height = 0 } = ref.current
    ? ref.current.getBoundingClientRect()
    : {};

  const { scrollY } = useViewportScroll();
  useEffect(() => {
    return scrollY.onChange(() => setY(scrollY.get()));
  }, [scrollY]);

  return (
    <Box pos="relative">
      <chakra.header
        bg={bg}
        borderTop="6px solid"
        borderTopColor={border_top}
        overflowY="hidden"
        ref={ref}
        shadow={y > height ? 'sm' : undefined}
        transition="box-shadow 0.2s"
        w="full"
      >
        <chakra.div h="4.5rem" mx="auto" maxW="1200px">
          <Flex w="full" h="full" px="6" align="center" justify="space-between">
            <Flex align="center">
              <Link to="/">
                <HStack>
                  <Image src={logo} width={[200, 180]} />
                </HStack>
              </Link>
            </Flex>

            <Flex
              align="center"
              color="gray.300"
              justify="flex-end"
              maxW="824px"
              w="full"
            >
              <IconButton
                aria-label={`Switch to ${text} mode`}
                color="current"
                fontSize="lg"
                icon={<SwitchIcon />}
                ml={{ base: '0', md: '3' }}
                onClick={toggleMode}
                size="md"
                variant="ghost"
              />
            </Flex>
          </Flex>
        </chakra.div>
      </chakra.header>
    </Box>
  );
};

export default Header;
