import PropTypes from 'prop-types';
import { Flex, SimpleGrid, Text } from '@chakra-ui/react';
import ItemCard from '../ItemCard';

const ItemList = ({ data, category }) => {
  return (
    <Flex justify="center" align="center">
      {(data.length === 0) && (
        <Text fontWeight={500} color="brand.100">Sorry, we found nothing</Text>
      )}
      {(data.length > 0) && (
        <SimpleGrid columns={[1, 2, 4]} spacing="25px" flex={1} maxW={['100%', '80%']} p="5">
          {data.map((item) => {
            const { id } = item;
            return (<ItemCard key={id} data={item} category={category} />);
          })}
        </SimpleGrid>
      )}
    </Flex>
  );
};

const { arrayOf, shape, string } = PropTypes;

ItemList.propTypes = {
  data: arrayOf(shape({})),
  category: string
};

export default ItemList;
