import PropTypes from 'prop-types';
import { Flex, ListItem, UnorderedList } from '@chakra-ui/react';
import { uid } from '../../Utils';

const CategoryInfo = ({ data }) => {
  const { items } = data;

  return (
    <Flex direction="column" mt={[5, '20px']} maxW="390px">
      <UnorderedList>
        {items.map((item) => {
          const { name } = item;
          return (
            <ListItem key={uid()}>
              {name}
            </ListItem>
          );
        })}
      </UnorderedList>
    </Flex>
  );
};

const { shape } = PropTypes;

CategoryInfo.propTypes = {
  data: shape({})
};

export default CategoryInfo;
