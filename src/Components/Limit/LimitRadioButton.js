import PropTypes from 'prop-types';
import { Box, useColorModeValue, useRadio } from '@chakra-ui/react';

const LimitRadioButton = ({ children, ...rest }) => {
  const { getInputProps, getCheckboxProps } = useRadio(rest);

  const input = getInputProps();
  const checkbox = getCheckboxProps();

  const bg = useColorModeValue('brand.100', '#3f444e');

  return (
    <Box as='label'>
      <input {...input} />
      <Box
        {...checkbox}
        cursor='pointer'
        px={3}
        py={1}
        style={{ userSelect: 'none' }}
        _checked={{
          bg,
          borderColor: 'brand.200',
          color: 'white',
          fontWeight: '600'
        }}
        _focus={{
          boxShadow: 'outline'
        }}
      >
        {children}
      </Box>
    </Box>
  );
};

const { oneOfType, node } = PropTypes;

LimitRadioButton.propTypes = {
  children: oneOfType([
    node.isRequired
  ]).isRequired
};

export default LimitRadioButton;
