import PropTypes from 'prop-types';
import { Flex, HStack, useRadioGroup } from '@chakra-ui/react';
import { LIMIT_DEFAULT_VALUE, LIMIT, MAX_INPUT_WIDTH } from '../../App';
import LimitRadioButton from './LimitRadioButton';

const Limit = ({ onChange }) => {
  const { getRootProps, getRadioProps, setValue } = useRadioGroup({
    name: 'limit',
    defaultValue: LIMIT_DEFAULT_VALUE,
  });

  const group = getRootProps();
  const handleLimitChange = (event) => {
    const { value } = event.target;
    setValue(Number(value));
    onChange(Number(value));
  };

  return (
    <Flex justify="flex-end" maxW={MAX_INPUT_WIDTH} alignSelf="center" w="100%">
      <HStack {...group}>
        {LIMIT.map(item => {
          const radio = getRadioProps({ value: item });
          return (
            <LimitRadioButton
              key={item}
              value={item}
              {...radio}
              onChange={handleLimitChange}
            >
              {item}
            </LimitRadioButton>
          );
        })}
      </HStack>
    </Flex>
  );
};

const { func } = PropTypes;

Limit.propTypes = {
  onChange: func
};

export default Limit;
